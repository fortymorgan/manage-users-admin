import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../actions';

const mapStateToProps = (state) => {
  const { users, fetched } = state;

  return { users, fetched };
};

class Users extends Component {
  onFetch = query => () => {
    const { fetchUsers } = this.props;
    fetchUsers(query);
  }

  onUserSelect = id => () => {
    const { fetchUser } = this.props;
    fetchUser(id);
  }

  render() {
    const { users: { list, pagination }, fetched } = this.props;
    const {
      count,
      next,
      prev,
      offset,
    } = pagination;

    return (
      fetched ? null
        : <div className="users">
            <table>
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Full name</th>
                  <th>E-mail</th>
                </tr>
              </thead>
              <tbody>
                {list.map(({ id, fullName, email }) => (
                  <tr key={id} className="user-row" onClick={this.onUserSelect(id)}>
                    <td>{id}</td>
                    <td>{fullName}</td>
                    <td>{email}</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="pagination">
              <div className="prev">
                <button disabled={prev === null} onClick={this.onFetch(prev)}>{'<<'}</button>
              </div>
              <div className="count">
                {`${offset + 10} / ${count}`}
              </div>
              <div className="next">
                <button disabled={next === null} onClick={this.onFetch(next)}>{'>>'}</button>
              </div>
            </div>
          </div>
    );
  }
}

export default connect(
  mapStateToProps,
  actionCreators,
)(Users);
