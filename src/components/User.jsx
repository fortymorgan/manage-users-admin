import React, { Component } from 'react';
import { connect } from 'react-redux';
import { YMaps, Map, GeoObject } from 'react-yandex-maps';
import * as actionCreators from '../actions';

const UserTable = (props) => {
  const {
    user: {
      id,
      firstName,
      lastName,
      email,
    },
  } = props;

  return (
    <table className="user-table">
      <tbody>
        <tr>
          <td>Id</td>
          <td>{id}</td>
        </tr>
        <tr>
          <td>First name</td>
          <td>{firstName}</td>
        </tr>
        <tr>
          <td>Last name</td>
          <td>{lastName}</td>
        </tr>
        <tr>
          <td>E-Mail</td>
          <td>{email}</td>
        </tr>
      </tbody>
    </table>
  );
};

const mapStateToProps = (state) => {
  const { user, fetched } = state;

  return { user, fetched };
};

class User extends Component {
  // componentDidMount() {
  //   const { user: { location } } = this.props;
  //   function init() {
  //     const map = new window.ymaps.Map('map', {
  //       center: location,
  //       controls: [],
  //       zoom: 11,
  //     });
  //     const user = new window.ymaps.GeoObject({
  //       geometry: {
  //         type: 'Point',
  //         coordinates: location,
  //       },
  //     });
  //     map.geoObjects.add(user);
  //   }
  //   window.ymaps.ready(init);
  // }

  render() {
    const { fetched, user } = this.props;
    const { location } = user;

    const coordsNumber = location.map(c => Number(c));

    const mapState = { center: coordsNumber, controls: [], zoom: 3 };

    return (
      fetched ? null
        : <div className="user">
            <UserTable user={user} />
            <div className="user-description">
              <textarea rows="10" cols="60"></textarea>
            </div>
            <div className="user-map">
              <YMaps>
                <Map
                  state={mapState}
                  width="100%"
                  height="20vh"
                >
                  <GeoObject
                    geometry={{
                      type: 'Point',
                      coordinates: coordsNumber,
                    }}
                  />
                </Map>
              </YMaps>
            </div>
          </div>
    );
  }
}

export default connect(
  mapStateToProps,
  actionCreators,
)(User);
