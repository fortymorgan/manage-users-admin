import React from 'react';
import { Route } from 'react-router';
import Users from './Users';
import User from './User';

const App = () => (
  <div>
    <Route exact path="/users" component={Users} />
    <Route path="/users/:id" component={User} />
  </div>
);

export default App;
