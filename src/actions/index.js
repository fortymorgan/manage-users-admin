import { createAction } from 'redux-actions';
import { push } from 'react-router-redux';
import token from '../token';
import routes from '../routes';

export const getUsers = createAction('USERS_GET');
export const getUser = createAction('USER_GET');

export const request = createAction('REQUEST');
export const success = createAction('SUCCESS');

export const fetchUsers = query => async (dispatch) => {
  dispatch(request());
  const users = await fetch(routes.users(query), {
    headers: {
      'X-Auth-Token': token,
    },
  }).then(blob => blob.json());

  dispatch(success());
  dispatch(getUsers(users));
};

export const fetchUser = id => async (dispatch) => {
  dispatch(request());
  const user = await fetch(routes.user(id), {
    headers: {
      'X-Auth-Token': token,
    },
  }).then(blob => blob.json());

  dispatch(success());
  dispatch(getUser(user));
  dispatch(push(`/users/${id}`));
};
