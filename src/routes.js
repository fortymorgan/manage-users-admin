export default {
  users: query => `https://front-test.now.sh/users${query || ''}`,
  user: id => `https://front-test.now.sh/users/${id}`,
};
