import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { Router } from 'react-router';
import { syncHistoryWithStore, push, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import App from './components/App';
import reducers from './reducers';
import * as actions from './actions';
import routes from './routes';
import token from './token';

export default async () => {
  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const history = createHistory();
  const routingMiddleware = routerMiddleware(history);

  const store = createStore(
    reducers,
    composeEnhancers(
      applyMiddleware(thunk),
      applyMiddleware(routingMiddleware),
    ),
  );

  syncHistoryWithStore(history, store);

  store.dispatch(push('/users'));

  const users = await fetch(routes.users(), {
    headers: {
      'X-Auth-Token': token,
    },
  }).then(blob => blob.json());
  store.dispatch(actions.getUsers(users));

  ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>,
    document.getElementById('container'),
  );
};
