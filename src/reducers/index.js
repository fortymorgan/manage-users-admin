import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import { routerReducer } from 'react-router-redux';
import * as actions from '../actions';

const fetched = handleActions({
  [actions.request]() {
    return true;
  },
  [actions.success]() {
    return false;
  },
}, false);

const users = handleActions({
  [actions.getUsers](state, { payload }) {
    const { data, pagination } = payload;

    const list = data.map(({ id, fullName, email }) => ({ id, fullName, email }));

    return { ...state, list, pagination };
  },
}, {});

const user = handleActions({
  [actions.getUser](state, { payload }) {
    const { data } = payload;
    const {
      id,
      firstName,
      lastName,
      email,
      location,
    } = data;

    return {
      id,
      firstName,
      lastName,
      email,
      location,
    };
  },
}, {});

export default combineReducers({
  fetched,
  users,
  user,
  routing: routerReducer,
});
